__author__ = 'sven'

def bytestring_to_int(converted_string):
    # your implementation here
    int_value = 0
    for character in converted_string:
        int_value = int_value << 8
        int_value = int_value | ord(character)
    return int_value

def int_to_bytestring(integer, length):
    array = range(length)
    for x in range(0, length):
        array[x] = (integer >> 8 * (length - x - 1)) & 0xff
    return ''.join([chr(x) for x in array])