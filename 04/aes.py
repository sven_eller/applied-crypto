#!/usr/bin/python

import datetime, os, sys
from pyasn1.type import univ, namedtype
from pyasn1.codec.der import encoder, decoder

# $ sudo apt-get install python-crypto
sys.path = sys.path[1:] # removes script directory from aes.py search path
from Crypto.Cipher import AES          # https://www.dlitz.net/software/pycrypto/api/current/Crypto.Cipher.AES-module.html
from Crypto.Protocol.KDF import PBKDF2 # https://www.dlitz.net/software/pycrypto/api/current/Crypto.Protocol.KDF-module.html#PBKDF2
import hashlib, hmac

block_size = 16

pad = lambda s: s + (block_size - len(s) % block_size) * chr(block_size - len(s) % block_size)
unpad = lambda s : s[0:-ord(s[-1])]


class AlgorithmIdentifier(univ.Sequence):
    componentType = namedtype.NamedTypes(
      namedtype.NamedType('algorithm', univ.ObjectIdentifier()),
      namedtype.OptionalNamedType('info', univ.Null())
    )

class AesInfo(univ.Sequence):
    componentType = namedtype.NamedTypes(
      namedtype.NamedType('algorithm', univ.ObjectIdentifier()),
      namedtype.NamedType('iv', univ.OctetString())
    )

class Pbkdf2Params(univ.Sequence):
    componentType = namedtype.NamedTypes(
      namedtype.NamedType('salt', univ.OctetString()),
      namedtype.NamedType('iterationCount', univ.Integer()),
      namedtype.NamedType('keyLength', univ.Integer())
    )

class DigestInfo(univ.Sequence):
    componentType = namedtype.NamedTypes(
      namedtype.NamedType('digestAlgorithm', AlgorithmIdentifier()),
      namedtype.NamedType('digest', univ.OctetString())
    )


class EncInfo(univ.Sequence):
    componentType = namedtype.NamedTypes(
      namedtype.NamedType('kdfInfo', AlgorithmIdentifier()),
      namedtype.NamedType('cipherInfo', AesInfo()),
      namedtype.NamedType('hmacInfo', DigestInfo())
    )


def _mac(filename, key):
    mack_finder = hmac.new(key, None, hashlib.sha1)
    file = open(filename)

    chunk_size = 512
    chunk = file.read(chunk_size)
    mack_finder.update(chunk)

    while chunk != "":
        # Do stuff with byte.
        chunk = file.read(chunk_size)
        mack_finder.update(chunk)

    return mack_finder.digest()
# this function benchmarks how many PBKDF2 iterations
# can be performed in one second on the machine it is executed
def benchmark():
    # measure time for performing 100 000 iterations
    # extrapolate to 1 second
    start = datetime.datetime.now()
    test_iter_count = 1
    PBKDF2('a', '1', 36, test_iter_count)
    stop = datetime.datetime.now()
    time = (stop-start).total_seconds()
    iter = int(test_iter_count / time)
    print "[+] Benchmark: %s PBKDF2 iterations in 1 second" % (iter)
    return iter

def bytestring_to_int(converted_string):
    # your implementation here
    int_value = 0
    for character in converted_string:
        int_value = int_value << 8
        int_value = int_value | ord(character)
    return int_value

def int_to_bytestring(integer, length):
    s = ""
    while length:
        s = chr(0xff & integer) + s
        integer >>= 8
        length -= 1
    return s


def _xor_strings(message, key):
    assert len(message) == len(key)
    return int_to_bytestring(bytestring_to_int(message) ^ bytestring_to_int(key), len(key))

def encrypt(pfile, cfile):

    # benchmarking
    iteration_count = benchmark()

    # asking for password
    file = open(pfile)
    print "[?] Enter password:"

    password = raw_input()
    # derieving key

    salt = os.urandom(8)
    init_vector = os.urandom(block_size)

    tmp_filename = cfile + '.tmp'
    tmp_file = open(tmp_filename, 'wb')

    # writing ciphertext in temporary file and calculating HMAC digest
    key = PBKDF2(password, salt, 36, iteration_count)
    aes_key = key[0:16]
    hmac_key = key[16:len(key)]

    cipher = AES.new(aes_key)
    new_vector = init_vector
    block = file.read(block_size)
    while block != "":
        if len(block) != block_size:
            block = pad(block)
        encrypted_block = cipher.encrypt(_xor_strings(block, new_vector))
        new_vector = encrypted_block
        tmp_file.write(encrypted_block)
        block = file.read(block_size)

    tmp_file.close()

    enc_info = EncInfo()
    pbkdf_params = Pbkdf2Params()
    pbkdf_params.setComponentByName('iterationCount', iteration_count)
    pbkdf_params.setComponentByName('keyLength', 36)
    pbkdf_params.setComponentByName('salt', salt)


    enc_info.setComponentByName('kdfInfo', pbkdf_params)

    aes_info = AesInfo()
    aes_info.setComponentByName('algorithm', univ.ObjectIdentifier('2.16.840.1.101.3.4.1.2'))
    aes_info.setComponentByName('iv', univ.OctetString(init_vector))

    enc_info.setComponentByName('cipherInfo', aes_info)
    digest_info = DigestInfo()
    digest_algorithm = AlgorithmIdentifier()
    digest_algorithm.setComponentByName('algorithm', univ.ObjectIdentifier('2.16.840.1.101.3.4.2.1'))
    digest_algorithm.setComponentByName('info', univ.Null())
    digest_info.setComponentByName('digestAlgorithm', digest_algorithm)
    digest_info.setComponentByName('digest', _mac(tmp_filename, hmac_key))

    enc_info.setComponentByName('hmacInfo', digest_info)

    # writing DER structure in cfile
    cypher_file = open(cfile, 'wb')
    cypher_file.write(encoder.encode(enc_info))
    # append temporary ciphertext file to cfile
    cypher_file.write(open(tmp_filename).read())

    # deleting temporary ciphertext file
    #os.remove(tmp_filename)
    pass


def decrypt(cfile, pfile):
    result = decoder.decode(open(cfile).read())
    length_of_header = len(encoder.encode(result[0]))

    # reading DER structure

    print "[?] Enter password:"
    # asking for password
    password = raw_input()

    salt = str(result[0][0][0])
    iteration_count = int(result[0][0][1])
    key_length = int(result[0][0][2])
    stored_hmac = result[0][2][1]
    init_vector = str(result[0][1][1])

    key = PBKDF2(password, salt, key_length, iteration_count)
    aes_key = key[0:16]

    hmac_key = key[16:len(key)]

    mack_finder = hmac.new(hmac_key, None, hashlib.sha1)
    mack_finder.update(result[1])
    calculated_hmac = mack_finder.digest()

    if stored_hmac != calculated_hmac:
        print "[-] HMAC verification failure: wrong password or modified ciphertext!"
        sys.exit(1)
    else:
        print "[+] HMAC verification successful!"


    encrypted_file = open(cfile)
    plaintext_file = open(pfile, 'wb')
    encrypted_file.read(length_of_header)

    cipher = AES.new(aes_key)
    new_vector = init_vector
    block = encrypted_file.read(block_size)
    plaintext = ''
    while block != "":
        decrypted_block = cipher.decrypt(block)
        plaintext += _xor_strings(decrypted_block, new_vector)
        new_vector = block
        block = encrypted_file.read(block_size)

    unpadded_plaintext = unpad(plaintext)
    plaintext_file.write(unpadded_plaintext)
    pass

def usage():
    print "Usage:"
    print "-encrypt <plaintextfile> <ciphertextfile>"
    print "-decrypt <ciphertextfile> <plaintextfile>"
    sys.exit(1)


if len(sys.argv) != 4:
    usage()
elif sys.argv[1] == '-encrypt':
    encrypt(sys.argv[2], sys.argv[3])
elif sys.argv[1] == '-decrypt':
    decrypt(sys.argv[2], sys.argv[3])
else:
    usage()
