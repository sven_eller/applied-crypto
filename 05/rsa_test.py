from  rsa import _is_pem_format, pem_to_der, get_pubkey

__author__ = 'sven'

import unittest

pem_pubkey = """-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC/cUiJnyLwxl92WVIR+SvlBO3h
ijReQzKGK8RDj0Bb6Gx0TQ6EmyF/hoh32tQu90BEsag8ki/0HKN41xQLiZbYiJv0
l5nnuEU7SKeshBpq+GPt1znp5W0vyKLwy86CJb9NH2XMjAeAnIMS9pxQYoNnWnHb
QNmSS88nFa24MFVVSQIDAQAB
-----END PUBLIC KEY-----"""

pem_privatekey = """-----BEGIN RSA PRIVATE KEY-----
MIICWwIBAAKBgQC/cUiJnyLwxl92WVIR+SvlBO3hijReQzKGK8RDj0Bb6Gx0TQ6E
myF/hoh32tQu90BEsag8ki/0HKN41xQLiZbYiJv0l5nnuEU7SKeshBpq+GPt1znp
5W0vyKLwy86CJb9NH2XMjAeAnIMS9pxQYoNnWnHbQNmSS88nFa24MFVVSQIDAQAB
AoGABO8NeIaCZwQlx64LoGz0haV33/0o1lkS/0Q3fao+dV/rE6A8xIwe8+NqtvQL
zd4PaPXPKSh3Veo7CE06tgVovsZjfskG6ffxuO+I+PYxhkLsWH+1zTPa5A013bkn
/nmzLEeSpwTvOkCMiGO4GXXJjFLy5HBtxlr5F4BWZOVeguUCQQD3PctjAHBVFvsp
ti4AxnFR8L57Jy7oPkE3WrCDMcUTYHNF1D4gHIZ+5zWVYelOMoj+U3hdVn9297U3
S4HWhOazAkEAxjl0dMI9NNjvcnjFHnYCX5NA7uXhFbktTT1/8HEkfPoPize83UEK
N0Gwx8Qm1M6naqLzSQfhdUkMtQVqrZHyEwJAKBJt6ofhrQjigGo0no+LQlqgpNk+
8wlUDKK4RStF49QYGtNBlnbJHbxfpOyPocrzBTWGJqqkUiXnyySD0L8NnQJAEwQJ
/vVvR7WT6uhZCJ4Lhj2rGf/lgt770JAYyOkhtfeLLDDRpmwJ3dkoa1GO5BVbDCjL
q/LclxLF7EuzuyjoDwJAIV81S1Ya/rz90zfPVuvEGntktyvu4hXzheIUFuGlo+p+
1/mpZ/66V18JbuyfrLNYvA+zfOVhqL4qQ7w0MwpxLQ==
-----END RSA PRIVATE KEY-----"""

class RsaTestCase(unittest.TestCase):

    def test_is_pem_format(self):
        self.assertTrue(_is_pem_format(pem_pubkey))
        self.assertTrue(_is_pem_format(pem_privatekey))
        self.assertFalse(_is_pem_format("priv.der"))
        self.assertFalse(_is_pem_format("pub.der"))

    def test_get_pubkey(self):
        print get_pubkey("pub.pem")



if __name__ == '__main__':
    unittest.main()

