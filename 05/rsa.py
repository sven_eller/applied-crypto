#!/usr/bin/env python
import base64

import hashlib, os, sys # do not use any other imports/libraries
from pyasn1.codec.der import decoder
from pyasn1.codec.der import encoder
from pyasn1.type import univ, namedtype

# specify here how much time your solution required
# 6

class AlgorithmIdentifier(univ.Sequence):
    componentType = namedtype.NamedTypes(
      namedtype.NamedType('algorithm', univ.ObjectIdentifier()),
      namedtype.NamedType('parameters', univ.Null())
    )

class DigestInfo(univ.Sequence):
    componentType = namedtype.NamedTypes(
      namedtype.NamedType('digestAlgorithm', AlgorithmIdentifier()),
      namedtype.NamedType('digest', univ.OctetString())
    )

def int_to_bytestring(i, length):
    # converts integer to bytestring
    s = ""
    for smth in xrange(length):
        s = chr(i & 0xff) + s
        i >>= 8
    return s

def bytestring_to_int(s):
    # converts bytestring to integer
    i = 0
    for char in s:
        i <<= 8
        i |= ord(char)
    return i

def int_size_in_bytes(integer):
    if integer == 0:
        return 1
    size = 0
    while integer > 0:
        integer >>= 8
        size += 1

    return size


def pem_to_der(keyfile):
    content = open(keyfile).read()
    if _is_pem_format(content):
        content = content.replace("-----BEGIN PUBLIC KEY-----", "").strip()\
            .replace("-----END PUBLIC KEY-----", "").strip()\
            .replace("-----BEGIN RSA PRIVATE KEY-----", "").strip()\
            .replace("-----END RSA PRIVATE KEY-----", "").strip()
        return base64.b64decode(content)
    else:
        return keyfile

def _is_pem_format(content):
    return content.startswith("-----BEGIN")

def pkcsv15pad_encrypt(plaintext, n):
    # pad plaintext for encryption according to PKCS#1 v1.5

    # calculate byte size of the modulus n
    key_length_in_bytes = int_size_in_bytes(n)
    # plaintext must be at least 11 bytes smaller than modulus
    assert key_length_in_bytes + 11 >= len(plaintext)

    # generate padding bytes
    padding_size = key_length_in_bytes - len(plaintext) - 3
    padding_string = os.urandom(key_length_in_bytes * 2).replace(chr(0x00), "")[0:padding_size]
    padded_plaintext = chr(0x00) + chr(0x02) + padding_string + chr(0x00) + plaintext

    return padded_plaintext

def pkcsv15pad_sign(plaintext, n):
    # pad plaintext for signing according to PKCS#1 v1.5

    # calculate byte size of the modulus n
    key_length_in_bytes = int_size_in_bytes(n)

    # plaintext must be at least 3 bytes smaller than modulus
    assert key_length_in_bytes + 3 >= len(plaintext)

    # generate padding bytes
    padding_size = key_length_in_bytes - len(plaintext) - 3
    padding_string = chr(0xFF) * padding_size
    # generate padding bytes
    padded_plaintext = chr(0x00) + chr(0x01) + padding_string + chr(0x00) + plaintext
    return padded_plaintext

def _read_public_key_info(der):
    key_info_bitstring = decoder.decode(der)
    string_repr = ''.join([str(x) for x in key_info_bitstring[0][1]])
    int_keyinfo = int(string_repr, 2)
    key_info = decoder.decode(int_to_bytestring(int_keyinfo, int_size_in_bytes(int_keyinfo)))
    modulus = key_info[0][0]
    public_exponent = key_info[0][1]
    return modulus, public_exponent


def pkcsv15pad_remove(plaintext):
    # removes PKCS#1 v1.5 padding
    return plaintext

def encrypt(keyfile, plaintextfile, ciphertextfile):
    key, exponent = _read_public_key_info(pem_to_der(keyfile))
    plaintext = open(plaintextfile).read()
    padded_plaintext = pkcsv15pad_encrypt(plaintext, key)
    plaintext_int = bytestring_to_int(padded_plaintext)
    cyphertext_int = pow(plaintext_int, long(exponent),  long(key))
    cyphertext = int_to_bytestring(cyphertext_int, int_size_in_bytes(key))
    open(ciphertextfile, 'wb').write(cyphertext)

def decrypt(keyfile, ciphertextfile, plaintextfile):
    cyphertext = open(ciphertextfile).read()
    der, other = decoder.decode(pem_to_der(keyfile))
    modulus = der[1]
    private_exponent = der[3]
    int_cyphertext = bytestring_to_int(cyphertext)
    plaintext = int_to_bytestring(pow(long(int_cyphertext), long(private_exponent) ,long(modulus)), len(cyphertext))
    position_of_plaintext = plaintext[1:].find('\x00') + 2
    unpadded_plaintext = plaintext[position_of_plaintext:]
    open(plaintextfile, 'wb').write(unpadded_plaintext)


def digestinfo_der(filename):
    # returns ASN.1 DER encoded DigestInfo structure containing SHA1 digest of file
    file = open(filename)
    digest = hashlib.sha1(file.read())
    digest_info = DigestInfo()
    sha1_digest = digest.digest()
    digest_info.setComponentByName('digest', sha1_digest)
    algorithm_identifier = AlgorithmIdentifier()
    algorithm_identifier.setComponentByName('algorithm', univ.ObjectIdentifier('1.3.14.3.2.26'))
    algorithm_identifier.setComponentByName('parameters', univ.Null())

    digest_info.setComponentByName('digestAlgorithm', algorithm_identifier)
    return encoder.encode(digest_info)


def sign(keyfile, filetosign, signaturefile):
    digestinfo = digestinfo_der(filetosign)
    der, reminder = decoder.decode(pem_to_der(keyfile))
    modulus = der[1]
    private_exponent = der[3]
    padded_der = pkcsv15pad_sign(digestinfo, modulus)

    #open('plaintext_sign.tmp', 'wb').write(padded_der)

    digestinfo_int = bytestring_to_int(padded_der)
    signature = pow(digestinfo_int, long(private_exponent), long(modulus))
    open(signaturefile, 'wb').write(int_to_bytestring(signature, len(padded_der)))
    pass

def verify(keyfile, signaturefile, filetoverify):
    sha1_digest = hashlib.sha1(open(filetoverify).read()).digest()

    signature_str = open(signaturefile).read()
    signature_int = bytestring_to_int(signature_str)



    key, exponent = _read_public_key_info(pem_to_der(keyfile))
    keylength = len(signature_str)

    plaintext_int = pow(signature_int, long(exponent), long(key))
    plaintext = int_to_bytestring(plaintext_int, keylength)

    position_of_plaintext = plaintext[1:].find('\x00') + 2
    unpadded_plaintext = plaintext[position_of_plaintext:]

    digestinfo, remaining = decoder.decode(unpadded_plaintext)

    #open('digestinfo.tmp.verify', 'wb').write(plaintext)

    signature_digest = digestinfo[1]
    if signature_digest == sha1_digest:
        print "Verified OK"
    else:
        print "Verification Failure"

    #print "SHA1 of file: %s " + repr(sha1_digest)
    #print "Hash in singature: %s " + repr(signature_digest)
    #print "Plaintext: %s " % repr(unpadded_plaintext)
    pass

def usage():
    print "Usage:"
    print "encrypt <public key file> <plaintext file> <output ciphertext file>"
    print "decrypt <private key file> <ciphertext file> <output plaintext file>"
    print "sign <private key file> <file to sign> <signature output file>"
    print "verify <public key file> <signature file> <file to verify>"
    sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) != 5:
        usage()
    elif sys.argv[1] == 'encrypt':
        encrypt(sys.argv[2], sys.argv[3], sys.argv[4])
    elif sys.argv[1] == 'decrypt':
        decrypt(sys.argv[2], sys.argv[3], sys.argv[4])
    elif sys.argv[1] == 'sign':
        sign(sys.argv[2], sys.argv[3], sys.argv[4])
    elif sys.argv[1] == 'verify':
        verify(sys.argv[2], sys.argv[3], sys.argv[4])
    else:
        usage()
