#!/usr/bin/env python

import argparse, hashlib, sys, datetime # do not use any other imports/libraries


# specify here how much time your solution required
# It took 7 hours

#My output
#/usr/bin/python2.7 /home/sven/PycharmProjects/one-time-pad/13/pow.py --difficulty 26
#[+] Solved in 39 sec (0.158413 Mhash/sec)
#[+] Input: 5376656e00000000005e4568
#[+] Solution: 0000008708c875bbb9159c85ee94ab1fe3a0d49ceaead850950f19119e38caff
#[+] Nonce: 6178152

identity = 'Sven'

def usage():
    print "Usage:"
    print "--difficulty <difficulty>"
    sys.exit(1)

def is_solved(digest):
    return (ord(digest[leading_zero_bytes_count]) >> (8 - reminder)) == 0 and (digest[0:leading_zero_bytes_count] == leading_bytes)

def int_to_bytestring(integer, length):
    s = ""
    while length:
        s = chr(0xff & integer) + s
        integer >>= 8
        length -= 1
    return s


#Aris UT 62613423
def solve(difficulty):
    start = datetime.datetime.now()
    nonce = 0
    while(True):
        input = (identity + int_to_bytestring(nonce,8)).encode('hex')
        hashlib_sha_ = hashlib.sha256(hashlib.sha256(input).digest())
        digest = hashlib_sha_.digest()
        if is_solved(digest):
            stop = datetime.datetime.now()
            time = (stop-start).total_seconds()
            print '[+] Solved in %d sec (%f Mhash/sec)' % (time, (nonce/time) / 1000000)
            print '[+] Input: %s' % input
            print '[+] Solution: %s' % hashlib_sha_.hexdigest()
            print '[+] Nonce: %d' % nonce
            sys.exit()
        nonce+=1

    print(difficulty)

if len(sys.argv) != 3:
    usage()
elif sys.argv[1] == '--difficulty':
    difficulty = int(sys.argv[2])
    leading_zero_bytes_count = int(difficulty / 8)
    reminder = difficulty % 8
    leading_bytes = '\x00' * leading_zero_bytes_count
    solve(difficulty)
else:
    usage()
