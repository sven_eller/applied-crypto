#!/usr/bin/python

import hashlib, sys
from pyasn1.type import univ, namedtype
from pyasn1.codec.der import encoder, decoder
from pyasn1.type import univ, namedtype, tag

sys.path = sys.path[1:] # removes script directory from my_hmac.py search path
import hmac # do not use any other imports/libraries

# specify here how much time your solution required

algorithms = {
    (1, 2, 840, 113549, 2, 5): {
        'label':'MD5',
        'function': hashlib.md5
    },
    (1, 3, 14, 3, 2, 26):
        {
            'label' : 'SHA1',
            'function' : hashlib.sha1
        },
    (2, 16, 840, 1, 101, 3, 4, 2, 1): {
            'label' : 'SHA256',
            'function' : hashlib.sha256
        }
}

class AlgorithmIdentifier(univ.Sequence):
    componentType = namedtype.NamedTypes(
      namedtype.NamedType('algorithm', univ.ObjectIdentifier())
    )

class DigestInfo(univ.Sequence):
    componentType = namedtype.NamedTypes(
      namedtype.NamedType('digestAlgorithm', AlgorithmIdentifier()),
      namedtype.NamedType('digest', univ.OctetString())
    )


def verify(filename):
    print "[+] Reading HMAC DigestInfo from", filename+".hmac"
    digest_file = open(filename+".hmac").read()
    digest = decoder.decode(digest_file)

    algorithm = digest[0][0][0]

    saved_digest = digest[0][1]
    print '[+]', algorithms[algorithm]['label'],'digest :', ''.join(x.encode('hex') for x in saved_digest)

    print "[?] Enter key:",

    file = open(filename)

    key = raw_input()
    mack_finder = hmac.new(key, None, algorithms[algorithm]['function'])

    chunk_size = 512
    chunk = file.read(chunk_size)
    mack_finder.update(chunk)

    while chunk != "":
        # Do stuff with byte.
        chunk = file.read(chunk_size)
        mack_finder.update(chunk)

    digest_calculated = mack_finder.digest()

    print '[+] Calculated', algorithms[algorithm]['label'] ,'digest :', ''.join(x.encode('hex') for x in digest_calculated)


    if digest_calculated != saved_digest:
        print "[-] Wrong key or message has been manipulated!"
    else:
        print "[+] HMAC verification successful!"



def mac(filename):
    print "[?] Enter key:",
    key = raw_input()

    mack_finder = hmac.new(key, None, hashlib.sha256)

    file = open(filename)

    chunk_size = 512
    chunk = file.read(chunk_size)
    mack_finder.update(chunk)

    while chunk != "":
        # Do stuff with byte.
        chunk = file.read(chunk_size)
        mack_finder.update(chunk)

    digest = mack_finder.digest()

    print "[+] Calculated HMAC-SHA256:", "".join("{:02x}".format(ord(c)) for c in digest)

    digest_info = DigestInfo()
    digest_info.setComponentByName('digest', digest)

    algorithm_identifier = AlgorithmIdentifier()
    algorithm_identifier.setComponentByName('algorithm', univ.ObjectIdentifier('2.16.840.1.101.3.4.2.1'))

    digest_info.setComponentByName('digestAlgorithm', algorithm_identifier)

    digest_info_file = open(filename + '.hmac', "wb")

    digest_info_file.write(encoder.encode(digest_info))

    print "[+] Writing HMAC DigestInfo to", filename+".hmac"


def usage():
    print "Usage:"
    print "-verify <filename>"
    print "-mac <filename>"
    sys.exit(1)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        usage()
    elif sys.argv[1] == '-mac':
        mac(sys.argv[2])
    elif sys.argv[1] == '-verify':
        verify(sys.argv[2])
    else:
        usage()