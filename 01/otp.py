#!/usr/bin/env python
import os, sys       # do not use any other imports/libraries
# specify here how much time your solution required
# It took about 4 hours


def bytestring_to_int(converted_string):
    # your implementation here
    int_value = 0
    for character in converted_string:
        int_value = int_value << 8
        int_value = int_value | ord(character)
    return int_value

def int_to_bytestring(integer, length):
    array = range(length)
    for x in range(0, length):
        array[x] = (integer >> 8 * (length - x - 1)) & 0xff
    return ''.join([chr(x) for x in array])

def _xor_strings(message, key):
    assert len(message) == len(key)
    return int_to_bytestring(bytestring_to_int(message) ^ bytestring_to_int(key), len(key))



def encrypt(plain_text_file, key_file_name, ciphertext_file_name):
    plain_text_string = open(plain_text_file).read()

    key = os.urandom(len(plain_text_string))
    encrypted = _xor_strings(plain_text_string,key)

    key_file = open(key_file_name, "w")
    key_file.write(key)
    key_file.close()

    cypertext_file = open(ciphertext_file_name, "w")
    cypertext_file.write(encrypted)

    pass


def decrypt(cfile, kfile, pfile):
    key_text_string = open(kfile).read()
    ciphertext_text_string = open(cfile).read()
    plaintext_string = _xor_strings(ciphertext_text_string, key_text_string)

    plaintext_file = open(pfile, "w")
    plaintext_file.write(plaintext_string)
    plaintext_file.close()

    pass

def usage():
    print "Usage:"
    print "encrypt <plaintext file> <output key file> <ciphertext output file>"
    print "decrypt <ciphertext file> <key file> <plaintext output file>"
    sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) != 5:
        usage()
    elif sys.argv[1] == 'encrypt':
        encrypt(sys.argv[2], sys.argv[3], sys.argv[4])
    elif sys.argv[1] == 'decrypt':
        decrypt(sys.argv[2], sys.argv[3], sys.argv[4])
    else:
        usage()