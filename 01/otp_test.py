import filecmp

from otp import bytestring_to_int, int_to_bytestring, _xor_strings, encrypt, decrypt


__author__ = 'sven'

import unittest


class MyTestCase(unittest.TestCase):

    def test_bytestring_to_int(self):
        self.assertEqual(97, bytestring_to_int('a'))
        self.assertEqual(6382147, bytestring_to_int('abC'))

    def test_int_to_bytestring(self):
        self.assertEqual('a', int_to_bytestring(97, 1))
        self.assertEqual('abC', int_to_bytestring(6382147, 3))

    def test_otp_encrypt(self):
        self.assertEqual(chr(0), _xor_strings('A',  'A'))
        self.assertEqual(chr(3), _xor_strings('A',  'B'))

    def test_encrypt_decrypt_symmetry(self):
        encrypt('test_file.txt', 'key_file.txt', 'crypted_file.txt')
        decrypt('crypted_file.txt', 'key_file.txt', 'decrypted_file.txt')
        self.assertTrue(filecmp.cmp('test_file.txt', 'decrypted_file.txt'))


if __name__ == '__main__':
    unittest.main()
