#!/usr/bin/env python
import sys   # do not use any other imports/libraries
# specify here how much time your solution required


# these tables may help you
from util import bytestring_to_int

table_cla = {0:'universal',1:'application',2:'context-specific',3:'private'}
table_constructed = {0:'primitive', 1:'constructed'}
table_tag = {
1:'BOOLEAN',
2:'INTEGER',
3:'BIT STRING',
4:'OCTET STRING',
5:'NULL',
6:'OBJECT IDENTIFIER',
16:'SEQUENCE',
17:'SET',
19:'PrintableString',
22:'IA5String',
23:'UTCTime',
}

# your function that parses DER structure (you can add additional arguments)
# and you can implement it by not using recursion if you can
# you parser must be able to parse at least DER encoded structure created by asn1_task1.py
def parse_der(der, space=""):
    pass

def _tag_to_string(der):
    first_byte = ord(_get_first_byte(der))
    _class = first_byte >> 7
    _constructed = (first_byte >> 6) & 0x40
    _tag = first_byte & 0x1F

    if _tag in table_tag:
        tagname = table_tag[_tag]
    else:
        tagname = "[%s]" % _tag

    return "%s, %s, %s" % (table_cla[_class], table_constructed[_constructed], tagname)

def is_container(der):
    return (_get_first_byte(der[:1]) in [chr(0x30), chr(0x31)]) | _is_tag_explicit(der)

def _is_tag_explicit(der):
    return bytestring_to_int(_get_first_byte(der)) >> 5 == 0b101

def _get_first_byte(der):
    return der[:1]

if __name__ == '__main__':
    der = open(sys.argv[1], 'r').read()
    parse_der(der)