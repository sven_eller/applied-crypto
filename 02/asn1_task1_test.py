# coding=utf-8
from pyasn1.codec.ber import encoder
from pyasn1.type import char
from pyasn1.type import univ
from asn1_task1 import asn1_boolean, asn1_len, asn1_integer, asn1_null, asn1_bitstring, asn1_octetstring, \
    asn1_objectidentifier, asn1_printablestring, asn1_utctime, asn1_sequence, asn1_set, int_size_in_bytes
from pyasn1.type import useful

import unittest


class AsnEncoderTestCase(unittest.TestCase):

    def test_boolean_to_der(self):
        self.assertEqual(encoder.encode(univ.Boolean(False)), asn1_boolean(False))
        self.assertEqual("\x01\x01\xff", asn1_boolean(True))

    def test_asn1_integer(self):
        self.assertEqual('02020080'.decode("hex"), asn1_integer(128))
        self.assertEqual('020200ff'.decode("hex"), asn1_integer(255))
        self.assertEqual('0203008000'.decode("hex"), asn1_integer(32768))

        self.assertEqual(encoder.encode(univ.Integer(128)), asn1_integer(128))
        self.assertEqual(encoder.encode(univ.Integer(255)), asn1_integer(255))
        self.assertEqual(encoder.encode(univ.Integer(32768)), asn1_integer(32768))
        self.assertEqual(encoder.encode(univ.Integer(-1)), asn1_integer(-1))
        self.assertEqual(encoder.encode(univ.Integer(-2)), asn1_integer(-2))
        self.assertEqual(encoder.encode(univ.Integer(-128)), asn1_integer(-128))
        self.assertEqual(encoder.encode(univ.Integer(-129)), asn1_integer(-129))
        self.assertEqual(encoder.encode(univ.Integer(-130)), asn1_integer(-130))
        self.assertEqual(encoder.encode(univ.Integer(-1000000)), asn1_integer(-1000000))

        self.assertEqual(encoder.encode(univ.Integer(0)), asn1_integer(0))
        self.assertEqual(encoder.encode(univ.Integer(10)), asn1_integer(10))
        self.assertEqual(encoder.encode(univ.Integer(15)), asn1_integer(15))
        self.assertEqual(encoder.encode(univ.Integer(16)), asn1_integer(16))
        self.assertEqual(encoder.encode(univ.Integer(90)), asn1_integer(90))
        self.assertEqual(encoder.encode(univ.Integer(126)), asn1_integer(126))
        self.assertEqual(encoder.encode(univ.Integer(127)), asn1_integer(127))
        self.assertEqual(encoder.encode(univ.Integer(70000)), asn1_integer(70000))
        self.assertEqual(encoder.encode(univ.Integer(7000000)), asn1_integer(7000000))

    def test_asn1_null(self):
        self.assertEqual(encoder.encode(univ.Null()), asn1_null())

    def test_asn1_bitstring(self):
        self.assertEqual('03020700'.decode('hex'), asn1_bitstring('0'))
        self.assertEqual('03020780'.decode('hex'), asn1_bitstring('1'))
        self.assertEqual('030202a8'.decode('hex'), asn1_bitstring('101010'))
        self.assertEqual('0303063fc0'.decode('hex'), asn1_bitstring('0011111111'))
        self.assertEqual('0303003fc0'.decode('hex'), asn1_bitstring('0011111111000000'))
        self.assertEqual(encoder.encode(univ.BitString("'010'B")), asn1_bitstring('010'))
        self.assertEqual(encoder.encode(univ.BitString("")), asn1_bitstring(''))
        self.assertEqual(encoder.encode(univ.BitString("'01'B")), asn1_bitstring('01'))
        self.assertEqual(encoder.encode(univ.BitString("'1'B")), asn1_bitstring('1'))
        self.assertEqual(encoder.encode(univ.BitString("'10'B")), asn1_bitstring('10'))
        self.assertEqual(encoder.encode(univ.BitString("'11111110'B")), asn1_bitstring('11111110'))
        self.assertEqual(encoder.encode(univ.BitString("'11111111'B")), asn1_bitstring('11111111'))
        self.assertEqual(encoder.encode(univ.BitString("'00000000'B")), asn1_bitstring('00000000'))
        self.assertEqual(encoder.encode(univ.BitString("'000000001111111111111111'B")), asn1_bitstring('000000001111111111111111'))

    def test_asn1_octetstring(self):
        self.assertEqual(encoder.encode(univ.OctetString('abc')), asn1_octetstring('abc'))
        self.assertEqual(encoder.encode(univ.OctetString('a')), asn1_octetstring('a'))
        self.assertEqual(encoder.encode(univ.OctetString('Bit Longer String')), asn1_octetstring('Bit Longer String'))

    def test_asn1_object_identifier(self):
        self.assertEqual(encoder.encode(univ.ObjectIdentifier('1.2.128')), asn1_objectidentifier([1, 2, 128]))
        self.assertEqual(encoder.encode(univ.ObjectIdentifier('1.2.840')), asn1_objectidentifier([1, 2, 840]))
        self.assertEqual(encoder.encode(univ.ObjectIdentifier('1.1.12.10')), asn1_objectidentifier([1,1,12,10]))
        self.assertEqual(encoder.encode(univ.ObjectIdentifier('1.2.840.5.1000000')), asn1_objectidentifier([1, 2, 840, 5, 1000000]))
        self.assertEqual(encoder.encode(univ.ObjectIdentifier('1.0.0.0')), asn1_objectidentifier([1,0,0,0]))
        self.assertEqual(encoder.encode(univ.ObjectIdentifier('1.1')), asn1_objectidentifier([1,1]))
        self.assertEqual(encoder.encode(univ.ObjectIdentifier('0.0')), asn1_objectidentifier([0,0]))

    def test_int_size_in_bytes(self):
        self.assertEqual(1, int_size_in_bytes(0))
        self.assertEqual(1, int_size_in_bytes(1))
        self.assertEqual(1, int_size_in_bytes(255))
        self.assertEqual(2, int_size_in_bytes(65535))
        self.assertEqual(3, int_size_in_bytes(65536))
        self.assertEqual(4, int_size_in_bytes(0xFFFFFFFF))
        self.assertEqual(5, int_size_in_bytes(0x100000000))

    def test_asn1_printablestring(self):
        self.assertEqual(encoder.encode(char.PrintableString('Good morning')), asn1_printablestring('Good morning'))
        self.assertEqual(encoder.encode(char.PrintableString('Õunapirukas')), asn1_printablestring('Õunapirukas'))
        self.assertEqual(encoder.encode(char.PrintableString('a' * 127)), asn1_printablestring('a' * 127))
        self.assertEqual(encoder.encode(char.PrintableString('a' * 150)), asn1_printablestring('a' * 150))
        self.assertEqual(encoder.encode(char.PrintableString('a' * 1500)), asn1_printablestring('a' * 1500))
        self.assertEqual(encoder.encode(char.PrintableString('a' * 15000)), asn1_printablestring('a' * 15000))

    def test_asn1_utctime(self):
        self.assertEqual(encoder.encode(useful.UTCTime('21229010100Z')), asn1_utctime('21229010100Z'))

    def test_asn1_sequence(self):
        sequence_of = univ.SequenceOf(univ.Integer())
        sequence_of.setComponentByPosition(0, 100)
        sequence_of.setComponentByPosition(1, 10)
        self.assertEqual(encoder.encode(sequence_of), asn1_sequence(asn1_integer(100) + asn1_integer(10)))

    def test_asn1_set(self):
        set_of = univ.SetOf(univ.Integer())
        set_of.setComponentByPosition(0, 100)
        set_of.setComponentByPosition(1, 10)
        self.assertEqual(encoder.encode(set_of), asn1_set(asn1_integer(100) + asn1_integer(10)))


    def test_asn1_len(self):
        self.assertEqual(chr(0x00), asn1_len(''))
        self.assertEqual(chr(0x01), asn1_len("a"))
        self.assertEqual(chr(0x0f), asn1_len("a" * 15))

        #self.assertEqual(asn1_len("a" * 127 ).encode('hex'), '8180')
        #self.assertEqual(asn1_len("a" * 83010004).encode('hex'), '8180')


if __name__ == '__main__':
    unittest.main()

