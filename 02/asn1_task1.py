#!/usr/bin/env python
import sys   # do not use any other imports/libraries
# specify here how much time your solution required
#This task took about 20 hours of work.


def int_to_bytestring(integer, length):
    s = ""
    while length:
        s = chr(0xff & integer) + s
        integer >>= 8
        length -= 1
    return s

def asn1_len(content):
    # helper function - should be used in other functions to calculate length octet(s)
    # content - bytestring that contains TLV content octet(s)
    # returns length (L) octet(s) for TLV
    content_length = len(content)
    if content_length <= 127:
        return int_to_bytestring(content_length, 1)
    else:
        number_of_additional_length_bytes = int_size_in_bytes(content_length)
        return int_to_bytestring(128 + number_of_additional_length_bytes, 1) + int_to_bytestring(content_length, number_of_additional_length_bytes)

def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = chr(0xff)
    else:
        bool = chr(0x00)
    return chr(0x01) + asn1_len(bool) + bool

def asn1_integer(value):
    bytes = []
    while 1:
        bytes.insert(0, value & 0xff)
        if value == 0 or value == -1:
            break
        value = value >> 8

    while len(bytes) > 1 and (bytes[0] == 0 and bytes[1] & 0x80 == 0 or bytes[0] == 0xff and bytes[1] & 0x80 != 0):
        del bytes[0]

    body = ''.join([chr(x) for x in bytes])
    return chr(0x02) + asn1_len(body) + body


def asn1_bitstring(bitstr):
    length = len(bitstr)
    unused_last_bits = -length % 8
    used_last_bits = 8 - unused_last_bits
    body = chr(00)

    if bitstr != '':
        last_byte = chr((int(bitstr[-used_last_bits:],2) << unused_last_bits) | 0x00)

        full_octets = ''
        if length > 8:
            full_octet_str_size = length - used_last_bits
            full_octet_str = bitstr[0:full_octet_str_size]
            full_octet_int = int(full_octet_str, 2)
            full_octets = int_to_bytestring(full_octet_int, (full_octet_str_size / 8))

        body = chr(unused_last_bits) + full_octets + last_byte

    return chr(0x03) + asn1_len(body) + body

def asn1_octetstring(octets):
    return chr(0x04) + asn1_len(octets) + octets

def asn1_null():
    return chr(0x05) + chr(0x00)

def int_size_in_bytes(integer):
    if integer == 0:
        return 1
    size = 0
    while integer > 0:
        integer >>= 8
        size += 1

    return size

def asn1_objectidentifier(oid):
    body = ''
    first_byte = chr(oid[0] * 40 + oid[1])
    body += first_byte
    for x in oid[2:len(oid)]:
        res = [ chr(x & 0x7f) ]
        x = x >> 7
        while x > 0:
            res.insert(0, chr(0x80 | (x & 0x7f)))
            x = x >> 7
        body += ''.join(res)
    return chr(0x06) + asn1_len(body) + body


def asn1_sequence(der):
    return chr(0x30) + asn1_len(der) + der

def asn1_set(der):
    return chr(0x31) + asn1_len(der) + der

def asn1_printablestring(string):
    return chr(0x13) + asn1_len(string) + string

def asn1_utctime(time):
    return chr(0x17) + asn1_len(time) + time

def asn1_tag_explicit(der, tag):
    return chr(tag | 0b10100000) + asn1_len(der) + der

# figure out what to put in '...' by looking on ASN.1 structure required
#asn1 = asn1_tag_explicit(asn1_sequence(asn1_boolean(True) + asn1_bitstring("010")), 0)
#asn1 = asn1_tag_explicit(asn1_sequence(asn1_boolean(True) + asn1_bitstring("010")), 0)

if __name__ == '__main__':
    root =  asn1_tag_explicit(asn1_sequence(
        asn1_set(
            asn1_integer(5) +
            asn1_tag_explicit(asn1_integer(200), 2) +
            asn1_tag_explicit(asn1_integer(-129), 11)
        ) +
        asn1_boolean(True) +
        asn1_bitstring("010")+
        asn1_octetstring(chr(00)+chr(01) +chr(02)*65)+
        asn1_null() +
        asn1_objectidentifier([1,2,840,113549,1]) +
        asn1_printablestring("hello.") +
        asn1_utctime('130123010900Z')
    ),0)

    open(sys.argv[1], 'w').write(root)
