# coding=utf-8

import unittest
from asn1_task2 import is_container, _get_first_byte, _is_tag_explicit, _tag_to_string
from asn1_task1 import asn1_boolean, asn1_null, asn1_set, asn1_sequence, asn1_tag_explicit, asn1_bitstring, \
    asn1_octetstring, asn1_objectidentifier, asn1_printablestring, asn1_utctime


class AsnDecoderTestCase(unittest.TestCase):

    def test_is_container(self):
        self.assertFalse(is_container(asn1_boolean(True)))
        self.assertFalse(is_container(asn1_boolean(False)))
        self.assertFalse(is_container(asn1_null()))
        self.assertTrue(is_container(asn1_sequence(asn1_null())))
        self.assertTrue(is_container(asn1_set(asn1_null())))

    def test_get_first_byte(self):
        self.assertEqual(chr(0x05), _get_first_byte(asn1_null()))

    def test_is_tag_explicit(self):
        self.assertTrue(_is_tag_explicit(asn1_tag_explicit(asn1_null(), 5)))
        self.assertFalse(_is_tag_explicit(asn1_null()))

    def test_tag_to_string(self):
        self.assertEqual(_tag_to_string(asn1_boolean(False)), "universal, primitive, BOOLEAN")
        self.assertEqual(_tag_to_string(asn1_bitstring('000000001111111111111111')), "universal, primitive, BIT STRING")
        self.assertEqual(_tag_to_string(asn1_octetstring("Octet string")), "universal, primitive, OCTET STRING")
        self.assertEqual(_tag_to_string(asn1_null()), "universal, primitive, NULL")
        self.assertEqual(_tag_to_string(asn1_objectidentifier([1,2,3])), "universal, primitive, OBJECT IDENTIFIER")
        self.assertEqual(_tag_to_string(asn1_printablestring("Print string")), "universal, primitive, PrintableString")
        self.assertEqual(_tag_to_string(asn1_utctime("21229010100Z")), "universal, primitive, UTCTime")
        self.assertEqual(_tag_to_string(asn1_tag_explicit(asn1_null(), 0)), "application, primitive, [0]")


if __name__ == '__main__':
    unittest.main()


