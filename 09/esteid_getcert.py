#!/usr/bin/env python

import argparse, sys     # do not use any other imports/libraries
from smartcard.CardType import AnyCardType
from smartcard.CardRequest import CardRequest
from smartcard.CardConnection import CardConnection
from smartcard.util import toHexString, HexListToBinString

# specify here how much time your solution required

parser = argparse.ArgumentParser(description='Fetch certificates from ID card', add_help=False)
parser.add_argument('--cert', type=str, default=None, choices=['auth','sign'], help='Which certificate to fetch')
parser.add_argument("--out", required=True, type=str, help="File to store certifcate (PEM)")
args = parser.parse_args()


# this will wait for card inserted in any reader
channel = CardRequest(timeout=100, cardType=AnyCardType()).waitforcard().connection
print "[+] Selected reader:", channel.getReader()

# using T=0 for compatibility and simplicity
channel.connect(CardConnection.T0_protocol)

# detect and print EstEID card type (EstEID spec page 14)
atr = channel.getATR()
if atr == [0x3B,0xFE,0x94,0x00,0xFF,0x80,0xB1,0xFA,0x45,0x1F,0x03,0x45,0x73,0x74,0x45,0x49,0x44,0x20,0x76,0x65,0x72,0x20,0x31,0x2E,0x30,0x43]:
    print "[+] EstEID v1.0 on Micardo Public 2.1"
elif atr == [0x3B,0xDE,0x18,0xFF,0xC0,0x80,0xB1,0xFE,0x45,0x1F,0x03,0x45,0x73,0x74,0x45,0x49,0x44,0x20,0x76,0x65,0x72,0x20,0x31,0x2E,0x30,0x2B]:
    print "[+] EstEID v1.0 on Micardo Public 3.0 (2006)"
elif atr == [0x3B,0x6E,0x00,0x00,0x45,0x73,0x74,0x45,0x49,0x44,0x20,0x76,0x65,0x72,0x20,0x31,0x2E,0x30]:
    print "[+] EstEID v1.1 on MultiOS (DigiID)"
elif atr == [0x3B,0xFE,0x18,0x00,0x00,0x80,0x31,0xFE,0x45,0x45,0x73,0x74,0x45,0x49,0x44,0x20,0x76,0x65,0x72,0x20,0x31,0x2E,0x30,0xA8]:
    print "[+] EstEID v3.x on JavaCard"
else:
    print "[-] Unknown card:", toHexString(atr)
    sys.exit()

def send(apdu):
    data, sw1, sw2 = channel.transmit(apdu)

    # success
    if [sw1,sw2] == [0x90,0x00]:
        return data
    # T=0 signals that there is more data to read
    elif sw1 == 0x61:
	print "[=] More data to read:", sw2
        return send([0x00, 0xC0, 0x00, 0x00, sw2]) # GET RESPONSE of sw2 bytes
    # probably error condition
    else:
        print "Error: %02x %02x, sending APDU: %s" % (sw1, sw2, toHexString(apdu))
        sys.exit()


def int_to_bytestring(integer, length):
    array = range(length)
    for x in range(0, length):
        array[x] = (integer >> 8 * (length - x - 1)) & 0xff
    return ''.join([chr(x) for x in array])

def bytestring_to_int(converted_string):
    # your implementation here
    int_value = 0
    for character in converted_string:
        int_value = int_value << 8
        int_value = int_value | ord(character)
    return int_value

# reading from card auth or sign certificate (EstEID spec page 33)
print "[=] Retrieving %s certificate..." % (args.cert)

# print all enteries in personal data file
send([0x00, 0xA4, 0x00, 0x0C]) # SELECT FILE (MF)
send([0x00, 0xA4, 0x01, 0x0C]+[0x02, 0xEE, 0xEE]) # MF/EEEE
if args.cert == 'auth':
    send([0x00, 0xA4, 0x02, 0x0C, 0x02, 0xAA, 0xCE]) # MF/EEEE/AACE
elif args.cert == 'sign':
    send([0x00, 0xA4, 0x02, 0x0C, 0x02, 0xDD, 0xCE]) # MF/EEEE/AACE

# read first 10 bytes to parse ASN.1 length field and determine certificate length
first_ten_bytes = send([0x00, 0xB0, 0x00, 0x00, 0x0A])

asn_body_length = 4
length_bytes = first_ten_bytes[3:asn_body_length]

certlen = bytestring_to_int([chr(first_ten_bytes[2]), chr(first_ten_bytes[3])])

f = open(args.out,"w")

offset = 0
cert_string = ''
print "Length: %d" % certlen

while offset < certlen:
    first_byte = int_to_bytestring(offset, 2)[0]
    second_byte = int_to_bytestring(offset, 2)[1]
    chunk_size = 0xFE
    response = send([0x00, 0xB0, ord(first_byte), ord(second_byte), chunk_size])
    cert_string += ''.join([chr(x) for x in response])
    offset += 0xFE

print "[+] Certificate size: %d bytes" % (certlen)
# save certificate in PEM form
open(args.out,"wb").write("-----BEGIN CERTIFICATE-----\n"+ cert_string[0: certlen + asn_body_length].encode('base64')+"-----END CERTIFICATE-----\n")
print "[+] Certificate stored in", args.out