#!/usr/bin/env python

import argparse, hashlib, datetime, re, socket, sys, os, urlparse  # do not use any other imports/libraries
from pyasn1.codec.der import decoder, encoder
from pyasn1.type import univ, namedtype, useful, char, tag

# specify here how much time your solution required

# parse arguments
parser = argparse.ArgumentParser(description='Check certificates against CRL', add_help=False)
parser.add_argument("url", type=str, help="URL of CRL (DER)")
parser.add_argument("--issuer", required=True, metavar='issuer', type=str, help="CA certificate that has issued certificates (DER/PEM)")
parser.add_argument("--certificates", required=True, metavar='cert', type=str, nargs='+', help="Certificates to check if revoked (DER/PEM)")
args = parser.parse_args()


def int_to_bytestring(i, length):
    # converts integer to bytestring
    s = ""
    for smth in xrange(length):
        s = chr(i & 0xff) + s
        i >>= 8
    return s

def bytestring_to_int(s):
    # converts bytestring to integer
    i = 0
    for char in s:
        i <<= 8
        i |= ord(char)
    return i

def int_size_in_bytes(integer):
    if integer == 0:
        return 1
    size = 0
    while integer > 0:
        integer >>= 8
        size += 1

    return size

def pem_to_der(content):
    # converts PEM content (if it is PEM) to DER
    if content[:2] == '--':
        content = content.replace("-----BEGIN CERTIFICATE REQUEST-----", "")
        content = content.replace("-----END CERTIFICATE REQUEST-----", "")
        content = content.replace("-----BEGIN CERTIFICATE-----", "")
        content = content.replace("-----END CERTIFICATE-----", "")
        content = content.replace("-----BEGIN PUBLIC KEY-----", "")
        content = content.replace("-----END PUBLIC KEY-----", "")
        content = content.replace("-----BEGIN RSA PRIVATE KEY-----", "")
        content = content.replace("-----END RSA PRIVATE KEY-----", "")
        content = content.decode('base64')
    return content

def get_pubkey_certificate(filename):
    # reads certificate and returns (n, e) from subject public key
    der, leftover = decoder.decode(pem_to_der(open(filename).read()))
    public_key_info = der[0][6][1]
    string_repr = ''.join([str(x) for x in public_key_info])
    int_keyinfo = int(string_repr, 2)

    key_info = decoder.decode(int_to_bytestring(int_keyinfo, int_size_in_bytes(int_keyinfo)))
    modulus = key_info[0][0]
    public_exponent = key_info[0][1]
    return (modulus, public_exponent)

def pkcsv15pad_remove(plaintext):
    # removes PKCS#1 v1.5 padding
    plaintext = plaintext[2:]
    if "\x00" in plaintext:
        return plaintext[plaintext.index("\x00")+1:]

def digestinfo_der(m):
    # returns ASN.1 DER encoded DigestInfo structure containing SHA1 digest of m
    sha1digest = hashlib.sha1(m).digest()

    class AlgorithmIdentifier(univ.Sequence):
        componentType = namedtype.NamedTypes(
            namedtype.NamedType('algorithm', univ.ObjectIdentifier()),
            namedtype.NamedType('parameters', univ.Null())
        )

    class DigestInfo(univ.Sequence):
        componentType = namedtype.NamedTypes(
            namedtype.NamedType('digestAlgorithm', AlgorithmIdentifier()),
            namedtype.NamedType('digest', univ.OctetString())
        )

    digestinfo = DigestInfo()
    algorithmidentifier = AlgorithmIdentifier()
    algorithmidentifier.setComponentByName('algorithm', "1.3.14.3.2.26")
    algorithmidentifier.setComponentByName('parameters', None)
    digestinfo.setComponentByName('digestAlgorithm', algorithmidentifier)
    digestinfo.setComponentByName('digest', sha1digest)
    der = encoder.encode(digestinfo)
    return der

def verify(keyfile, signature, contenttoverify):
    # returns 1 on "Verified OK" and 0 otherwise
    content = encoder.encode(contenttoverify)
    sha1_of_content = hashlib.sha1(content).digest()

    keylength = int_size_in_bytes(signature)

    key, exponent = get_pubkey_certificate(keyfile)
    plaintext_int = pow(signature, long(exponent), long(key))
    plaintext = pkcsv15pad_remove(int_to_bytestring(plaintext_int, keylength))

    try:
        digestinfo,reminder = decoder.decode(plaintext)
    except:
        #If digestinfo is not parsable then signature is invalid
        return 0

    if sha1_of_content == digestinfo[1]:
        return 1
    return 0


# list of serial numbers to check
serials = []


# download CRL using python sockets
def download_crl(url):
    print "[+] Downloading", url
    # parsing url
    host = urlparse.urlparse(url).netloc
    path = urlparse.urlparse(url).path

    # get response header
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, 80))

    get_str = "GET %s HTTP/1.1\r\nHost: %s \r\n\r\n" % (path, host)
    s.send(get_str)

    # read HTTP header header
    buf = s.recv(1)
    header = buf
    while not header.endswith("\r\n\r\n"):
        buf = s.recv(1)
        header += buf

    import re
    content_len = int(re.search('content-length:\s*(\d+)\s', header, re.S+re.I).group(1))

    body = ''
    i = 0
    while i < content_len:
        body += s.recv(1)
        i+=1

    return body

# verify if the CRL is signed by the issuer and whether the CRL is fresh
def verify_crl(issuer, crl):
    der, leftover = decoder.decode(crl)
    signature = _bitstring_to_int(der[2])
    tbsCertList = der[0]
    if verify(issuer, signature, tbsCertList):
        print "[+] CRL signature check successful!"
    else:
        print "[-] CRL signature verification failed!"
        sys.exit(1)

    thisUpdate = datetime.datetime.strptime(''.join(der[0][3]), '%y%m%d%H%M%SZ')
    nextUpdate = datetime.datetime.strptime(''.join(der[0][4]), '%y%m%d%H%M%SZ')

    now = datetime.datetime.utcnow()
    if now < thisUpdate or now > nextUpdate:
        print "[-] CRL outdated (nextUpdate: %s) (now: %s)" % (nextUpdate, now)
        sys.exit(1)

def _bitstring_to_int(bitstring):
    string_repr = ''.join([str(x) for x in bitstring])
    return int(string_repr, 2)


# verify if the certificates are signed by the issuer and add them to the list 'serials'
def load_serials(issuer, certificates):
    global serials
    for certificate in args.certificates:
        der, reminder = decoder.decode(pem_to_der(open(certificate).read()))
        signature = _bitstring_to_int(der[2])
        tbsCertificate = der[0]
        serial = der[0][1]

        if verify(issuer, signature, tbsCertificate):
            print "[+] Serial %s (%s) loaded" % (serial, certificate)
            serials.append(serial)
        else:
            print "[-] Serial %s (%s) not loaded: not issued by CA" % (serial, certificate)


# check if the certificates are revoked
# if revoked -- print revocation date and reason (if available)
def check_revoked(crl):
    global serials

    CRLReason = {
	0: 'unspecified',
	1: 'keyCompromise',
	2: 'cACompromise',
	3: 'affiliationChanged',
	4: 'superseded',
	5: 'cessationOfOperation',
	6: 'certificateHold',
	8: 'removeFromCRL',
	9: 'privilegeWithdrawn',
	10: 'aACompromise',
	}

    # loop over revokedCerts
    for revokedCert in decoder.decode(crl)[0][0][5]:
        serial = revokedCert[0]
        revocationDate = datetime.datetime.strptime(''.join(revokedCert[1]), '%y%m%d%H%M%SZ')
        reason = CRLReason[decoder.decode(''.join(revokedCert[2][0][1]))[0]]
        # if revoked serial is in our interest
        if serial in serials:
            print "[-] Certificate %s revoked: %s (%s)" % (serial, revocationDate, reason)

# download CRL
crl = download_crl(args.url)
# verify CRL
verify_crl(args.issuer, crl)

# load serial numbers from valid certificates
load_serials(args.issuer, args.certificates)

# check revocation status
check_revoked(crl)
