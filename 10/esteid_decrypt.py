#!/usr/bin/env python

import argparse, datetime, hashlib, sys     # do not use any other imports/libraries
from smartcard.CardType import AnyCardType
from smartcard.CardRequest import CardRequest
from smartcard.CardConnection import CardConnection
from smartcard.util import toHexString, HexListToBinString
from pyasn1.type import univ, namedtype
from pyasn1.codec.der import encoder

# parse arguments
parser = argparse.ArgumentParser(description='Perform decryption with ID card', add_help=False)
parser.add_argument("encrypted_file", type=str, help="File to decrypt")
parser.add_argument("--measure", default=None, action="store_true", help="Measure the time required to perform 100 operations")
args = parser.parse_args()

# specify here how much time your solution required

# this will wait for card inserted in any reader
channel = CardRequest(timeout=100, cardType=AnyCardType()).waitforcard().connection
print "[+] Selected reader:", channel.getReader()

# using T=0 for compatibility and simplicity
channel.connect(CardConnection.T0_protocol)

# detect and print EstEID card type (EstEID spec page 15)
atr = channel.getATR()
if atr == [0x3B,0xFE,0x94,0x00,0xFF,0x80,0xB1,0xFA,0x45,0x1F,0x03,0x45,0x73,0x74,0x45,0x49,0x44,0x20,0x76,0x65,0x72,0x20,0x31,0x2E,0x30,0x43]:
    print "[+] EstEID v1.0 on Micardo Public 2.1"
elif atr == [0x3B,0xDE,0x18,0xFF,0xC0,0x80,0xB1,0xFE,0x45,0x1F,0x03,0x45,0x73,0x74,0x45,0x49,0x44,0x20,0x76,0x65,0x72,0x20,0x31,0x2E,0x30,0x2B]:
    print "[+] EstEID v1.0 on Micardo Public 3.0 (2006)"
elif atr == [0x3B,0x6E,0x00,0x00,0x45,0x73,0x74,0x45,0x49,0x44,0x20,0x76,0x65,0x72,0x20,0x31,0x2E,0x30]:
    print "[+] EstEID v1.1 on MultiOS (DigiID)"
elif atr == [0x3B,0xFE,0x18,0x00,0x00,0x80,0x31,0xFE,0x45,0x45,0x73,0x74,0x45,0x49,0x44,0x20,0x76,0x65,0x72,0x20,0x31,0x2E,0x30,0xA8]:
    print "[+] EstEID v3.x on JavaCard"
else:
    print "[-] Unknown card:", toHexString(atr)
    sys.exit()

def send(apdu):
    data, sw1, sw2 = channel.transmit(apdu)

    # success
    if [sw1,sw2] == [0x90,0x00]:
        return data
    # T=0 signals that there is more data to read
    elif sw1 == 0x61:
        return send([0x00, 0xC0, 0x00, 0x00, sw2]) # GET RESPONSE of sw2 bytes
    # probably error condition
    else:
        print "Error: %02x %02x, sending APDU: %s" % (sw1, sw2, toHexString(apdu))
        sys.exit()

def measure():
    pass

if args.measure:
    measure()
    sys.exit(0)

cyphertext = open(args.encrypted_file).read()

print "[?] Enter PIN2:",
pin2 = raw_input()

send([0x00, 0xA4, 0x00, 0x0C]) # SELECT FILE (MF)
send([0x00, 0xA4, 0x01, 0x0C]+[0x02, 0xEE, 0xEE]) # MF/EEEE
send([0x00, 0x22, 0xF3, 0x06])# Select env. no 6

send([0x00, 0x22, 0x41, 0xA4, 0x02, 0x83, 0x00]) #Blah 1
send([0x00, 0x22, 0x41, 0xB6, 0x02, 0x83, 0x00]) #Blah 2
send([0x00, 0x22, 0x41, 0xB8, 0x05, 0x83, 0x03, 0x80, 0x01, 0x00]) #Blah 4


pinchars = [ord(c) for c in pin2]
cyphertext_list = [ord(c) for c in cyphertext]

send([0x00, 0x20, 0x00, 0x02, len(pin2)] + pinchars) #Send pin

dec = send([0x00, 0x2A, 0x80, 0x86, 0x81, 0x00] + cyphertext_list)

print "[+] Decrypted text:", HexListToBinString(dec).strip()
