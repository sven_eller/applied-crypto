#!/usr/bin/env python

import argparse, datetime, hashlib, sys     # do not use any other imports/libraries
from smartcard.CardType import AnyCardType
from smartcard.CardRequest import CardRequest
from smartcard.CardConnection import CardConnection
from smartcard.util import toHexString, HexListToBinString
from pyasn1.type import univ, namedtype
from pyasn1.codec.der import encoder

# specify here how much time your solution required

class AlgorithmIdentifier(univ.Sequence):
    componentType = namedtype.NamedTypes(
      namedtype.NamedType('algorithm', univ.ObjectIdentifier()),
      namedtype.NamedType('parameters', univ.Null())
    )

class DigestInfo(univ.Sequence):
    componentType = namedtype.NamedTypes(
      namedtype.NamedType('digestAlgorithm', AlgorithmIdentifier()),
      namedtype.NamedType('digest', univ.OctetString())
    )


# parse arguments
parser = argparse.ArgumentParser(description='Perform signing with ID card', add_help=False)
parser.add_argument("filetosign", type=str, help="File to sign")
parser.add_argument("signature", type=str, help="File to store signature")
parser.add_argument("--measure", default=None, action="store_true", help="Measure the time required to perform 100 operations")
args = parser.parse_args()


# this will wait for card inserted in any reader
channel = CardRequest(timeout=100, cardType=AnyCardType()).waitforcard().connection
print "[+] Selected reader:", channel.getReader()

# using T=0 for compatibility and simplicity
channel.connect(CardConnection.T0_protocol)

# detect and print EstEID card type (EstEID spec page 15)
atr = channel.getATR()
if atr == [0x3B,0xFE,0x94,0x00,0xFF,0x80,0xB1,0xFA,0x45,0x1F,0x03,0x45,0x73,0x74,0x45,0x49,0x44,0x20,0x76,0x65,0x72,0x20,0x31,0x2E,0x30,0x43]:
    print "[+] EstEID v1.0 on Micardo Public 2.1"
elif atr == [0x3B,0xDE,0x18,0xFF,0xC0,0x80,0xB1,0xFE,0x45,0x1F,0x03,0x45,0x73,0x74,0x45,0x49,0x44,0x20,0x76,0x65,0x72,0x20,0x31,0x2E,0x30,0x2B]:
    print "[+] EstEID v1.0 on Micardo Public 3.0 (2006)"
elif atr == [0x3B,0x6E,0x00,0x00,0x45,0x73,0x74,0x45,0x49,0x44,0x20,0x76,0x65,0x72,0x20,0x31,0x2E,0x30]:
    print "[+] EstEID v1.1 on MultiOS (DigiID)"
elif atr == [0x3B,0xFE,0x18,0x00,0x00,0x80,0x31,0xFE,0x45,0x45,0x73,0x74,0x45,0x49,0x44,0x20,0x76,0x65,0x72,0x20,0x31,0x2E,0x30,0xA8]:
    print "[+] EstEID v3.x on JavaCard"
else:
    print "[-] Unknown card:", toHexString(atr)
    sys.exit()

def send(apdu):
    data, sw1, sw2 = channel.transmit(apdu)
    
    # success
    if [sw1,sw2] == [0x90,0x00]:
        return data
    # T=0 signals that there is more data to read
    elif sw1 == 0x61:
        return send([0x00, 0xC0, 0x00, 0x00, sw2]) # GET RESPONSE of sw2 bytes
    # probably error condition
    else:
        print "Error: %02x %02x, sending APDU: %s" % (sw1, sw2, toHexString(apdu))
        sys.exit()


def digestinfo_der(filename):
    # returns ASN.1 DER encoded DigestInfo structure containing SHA1 digest of file
    file = open(filename)
    digest = hashlib.sha1(file.read())
    digest_info = DigestInfo()
    sha1_digest = digest.digest()
    digest_info.setComponentByName('digest', sha1_digest)
    algorithm_identifier = AlgorithmIdentifier()
    algorithm_identifier.setComponentByName('algorithm', univ.ObjectIdentifier('1.3.14.3.2.26'))
    algorithm_identifier.setComponentByName('parameters', univ.Null())

    digest_info.setComponentByName('digestAlgorithm', algorithm_identifier)
    return encoder.encode(digest_info)

print "[=] Signing file %s" % (args.filetosign)

sha1digest = hashlib.sha1(open(args.filetosign).read()).digest()

print "[+] Calculated SHA1 digest:", sha1digest.encode('hex')

# making asn1 hash object to sign according to PKCS#1 v1.5
print "[+] Making DigestInfo object..."

digestinfo = digestinfo_der(args.filetosign)

def measure():
    pass


if args.measure:
    measure()
    sys.exit(0)


# calculating the signature (EstEID spec page 40)
print "[+] Sending DigestInfo to smart card for signing..."
print "[?] Enter PIN2:",
pin2 = raw_input()

send([0x00, 0xA4, 0x00, 0x0C]) # SELECT FILE (MF)
send([0x00, 0xA4, 0x01, 0x0C]+[0x02, 0xEE, 0xEE]) # MF/EEEE
send([0x00, 0x22, 0xF3, 0x01])# Select env. no 1

pinchars = [ord(c) for c in pin2]
digest_as_list = [ord(c) for c in digestinfo]

pin_command = [0x00, 0x20, 0x00, 0x02, len(pin2)] + pinchars
send(pin_command)

sign_command = [0x00, 0x2A, 0x9E, 0x9A, 0x23] + digest_as_list
send([0x00, 0x22, 0xF3, 0x01])# Select env. no 1

signature = send(sign_command) # Sign

# save signature into file
print "[+] Signature saved into", args.signature

open(args.signature, 'wb').write(''.join([chr(x) for x in signature]))

